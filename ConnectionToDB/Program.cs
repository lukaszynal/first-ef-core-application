﻿#pragma warning disable IDE0063

using System;
using System.Linq;

namespace ConnectionToDB
{
    public static class Program
    {
        static void Main()
        {
            ReadData();
        }

        private static void ReadData()
        {
            using (FirstefdatabaseContext db = new FirstefdatabaseContext())
            {
                var users = db.Users.ToList();
                Console.WriteLine("List of users:");
                foreach (User user in users)
                {
                    Console.WriteLine($"{user.Id}.{user.Name} - {user.Address}");
                }
            }
        }
    }
}
