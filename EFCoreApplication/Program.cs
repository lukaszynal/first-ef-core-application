﻿#pragma warning disable CS8600
#pragma warning disable CS8602
#pragma warning disable CS8604
#pragma warning disable IDE0063

using System;
using System.Linq;

namespace EFCoreApplication
{
    public static class Program
    {
        static void Main()
        {
            AddUsers();
            UpdateUser();
        }

        private static void AddUsers()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                User user1 = new User { Name = "John", Address = "Sunny Street 1" };
                User user2 = new User { Name = "Alice", Address = "Sunny Street 2" };

                db.Users.AddRange(user1, user2);

                db.SaveChanges();
                Console.WriteLine("Sucsessfully stored");

                var users = db.Users.ToList();
                Console.WriteLine("List of users:");
                foreach (User user in users)
                {
                    Console.WriteLine($"{user.ID}.{user.Name} - {user.Address}");
                }
            }
        }

        private static void UpdateUser()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                User user = db.Users.FirstOrDefault();
                if (user != null)
                {
                    user.Name = "James";
                    user.Address = "John Street 1";

                    db.Users.Update(user);
                    db.SaveChanges();
                }

                Console.WriteLine("Updated users:");
                var users = db.Users.ToList();
                foreach (User i in users)
                {
                    Console.WriteLine($"{i.ID}.{i.Name} - {i.Address}");
                }
            }
        }
    }
}
